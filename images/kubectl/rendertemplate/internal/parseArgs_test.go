package internal

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestUserData(t *testing.T) {
	d := make(userData)

	require.NotNil(t, d.Set("a"))
	require.Nil(t, d.Set("d=e"))
	require.Nil(t, d.Set("b=c"))

	require.Equal(t, "b=c,d=e", d.String())
}

func TestParseArgs(t *testing.T) {
	os.Args = []string{"binary", "-v", "lalla"}
	a1, err := ParseArgs()
	require.NoError(t, err)

	a2, err := ParseArgs()
	require.NoError(t, err)
	require.Equal(t, a1, a2)

}
