package internal

import (
	"os"
	"regexp"
	"strings"
	"text/template"
)

// Slug cleans up a string and makes it ready to use in dns names
// often needed in kubernetes manifests
// much like the *_SLUG CI variables in gitlab
// https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
// lowercased, shortened or maxLength (disabled if -1, defaults to 63), and with everything except 0-9 and a-z replaced with -. No leading / trailing -
func Slug(maxLength int, inputs ...string) string {
	if maxLength == 0 {
		maxLength = 63
	}
	r := regexp.MustCompile(`[^a-zA-Z0-9]+`)

	input := strings.Join(inputs, "")

	output := r.ReplaceAllString(input, "-")
	output = strings.ToLower(output)
	output = strings.TrimLeft(output, "-")
	if maxLength != -1 && len(output) > maxLength {
		output = output[:maxLength]
	}
	output = strings.TrimRight(output, "-")
	return output
}

// Env works like os.Getenv but panics when the environment key does not exist or is empty
func Env(key string) string {
	v := os.Getenv(key)
	if v == "" {
		panic("no environment variable named " + key)
	}
	return v
}

// EnvOr works like os.Getenv but returns a default value when the environment key does not exist or is empty
func EnvOr(key string, defaultValue string) string {
	v := os.Getenv(key)
	if v == "" {
		return defaultValue
	}
	return v
}

// IfEnv returns true when a given environment value exists
func IfEnv(key string) bool {
	return os.Getenv(key) != ""
}

// SlugEnv is a convenience function combining Slug(Env())
func SlugEnv(maxLength int, key string) string {
	return Slug(maxLength, Env(key))
}

// FuncMap can be used in template.New().Funcs(FuncMap)
func FuncMap() template.FuncMap {
	return template.FuncMap{
		"slug":    Slug,
		"env":     Env,
		"slugEnv": SlugEnv,
		"envOr":   EnvOr,
		"ifEnv":   IfEnv,
	}
}
