package internal

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetTemplateValues(t *testing.T) {
	values := GetTemplateValues(map[string]string{"a": "b"})

	require.NotNil(t, values)
	require.Equal(t, "b", values.UserData["a"])
}

func TestGetGitlabFlowImageName(t *testing.T) {
	os.Setenv("CI_REGISTRY_IMAGE", "CI_REGISTRY_IMAGE")
	os.Setenv("CI_COMMIT_TAG", "CI_COMMIT_TAG")
	os.Setenv("CI_COMMIT_REF_NAME", "CI_COMMIT_REF_NAME")
	os.Setenv("CI_COMMIT_SHORT_SHA", "CI_COMMIT_SHORT_SHA")

	name := getGitlabFlowImageName()
	require.Equal(t, "CI_REGISTRY_IMAGE:tag-CI_COMMIT_TAG", name)

	os.Unsetenv("CI_COMMIT_TAG")
	name = getGitlabFlowImageName()
	require.Equal(t, "CI_REGISTRY_IMAGE:CI_COMMIT_REF_NAME-CI_COMMIT_SHORT_SHA", name)
}
