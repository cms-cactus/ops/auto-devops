package main

import (
	"io/ioutil"
	"os"
	"path"
	"strings"

	"rendertemplate/internal"
	"text/template"

	log "github.com/sirupsen/logrus"
)

func main() {
	args, err := internal.ParseArgs()
	if err != nil {
		log.WithError(err).Fatal("invalid arguments")
	}
	fromStdin := args.Filename == "-"
	rawTemplate := internal.GetTemplate(path.Base(args.Filename))
	var t *template.Template
	if fromStdin {
		if args.Verbose {
			log.Info("reading template from stdin")
		}
		bytes, _ := ioutil.ReadAll(os.Stdin)
		t, err = rawTemplate.Parse(string(bytes))
	} else {
		if args.Verbose {
			log.WithField("filename", args.Filename).Info("reading template from file")
		}
		t, err = rawTemplate.ParseFiles(args.Filename)
	}
	if err != nil {
		log.WithError(err).Fatal("could not parse template")
	}
	if args.Verbose {
		log.Info("template parsed")
	}

	{
		ciKeys := getCIKeys()
		if args.Verbose {
			log.WithField("keys", ciKeys).Info("available CI environment keys")
		}
		if len(ciKeys) == 0 {
			log.Warn("not running in CI, functions relying on CI env variables will fail")
		}
	}

	err = t.Execute(os.Stdout, internal.GetTemplateValues(args.UserData))
	if err != nil {
		log.WithError(err).Fatal("could not fill template")
	}
	if args.Verbose {
		log.Info("template processed")
	}
}

func getCIKeys() []string {
	ciKeys := make([]string, 0)
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		key := pair[0]
		// envKeys = append(envKeys, key)
		if strings.HasPrefix("CI_", key) {
			ciKeys = append(ciKeys, key)
		}
	}
	return ciKeys
}
