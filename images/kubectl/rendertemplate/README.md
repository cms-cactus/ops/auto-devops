# rendertemplate

A relatively simple renderer designed for easy templating of kubernetes manifests in Gitlab CI.

```bash
# basic example
$ rendertemplate template.yml
# verbose, custom user data. ex: {{ .UserData["customkey"] }}
$ rendertemplate -v --data customkey=value template.yml
```

## available data
The [data schema](internal/templateValues.go) available in templates has the following values:
- UserData (map of strings)
Contains data key=value pairs given as command line arguments (`--data key=value`)

## available functions
You have access to:
- [all built-in functions](https://golang.org/pkg/text/template/#hdr-Functions)
- [sprig functions](http://masterminds.github.io/sprig/)
- `env(key string)` retrieves an environment variable, errors when not found  
  example: `{{ env "CI_PROJECT_NAME" }}` -> `myproject`  
  you can use any environment variable you like, including [CI predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- `slug(maxLength int, inputs ...string)` sanitizes input for use in kubernetes manifests  
  example: `{{ slug 10 "_-HelLo, +--World!" }}` -> `hello-worl`  
  most keys (names, labels, annotations, ...) need a valid DNS-like value.  
  This means only lowercase alphanumeric (a-z0-9) characters and hyphens (-), and cannot have leading or trailing hyphens.  
  The value can also only be 63 characters long.  
  `slug` provides this conversion, and allows to specify the max length.
  `0` defaults to 63, `-1` disables length trimming.
  Note that `slug` can take an arbitrary amount of inputs, they will be concatenated.
- `slugEnv(maxLength int, key string)` a combination of `slug` and `env`
  example: `{{ slugEnv 0 "CI_PROJECT_NAME" }}` -> `myproject`
- `envOr(key string, defaultValue string)` retrieves an environment variable, returns `defaultValue` when not found or empty.
- `ifEnv(key string)` returns true when an environment variable is set and not empty