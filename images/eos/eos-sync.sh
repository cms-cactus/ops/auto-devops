#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
SCRIPTROOT="$(dirname "${BASH_SOURCE[0]:-$0}")/"

source ${SCRIPTROOT}/eos-env.sh

echo "copying $(pwd)/* to ${EOS_MGM_URL}${EOS_FOLDER}"
set -x
ls -l
eos mkdir -p $EOS_FOLDER
# eos cp -r * $EOS_FOLDER # does not work, '-r' causes access denied error for no good reason, even with owner account and no subfolders
find -type f -printf '%P\0' | xargs -0 -n1 -I {} bash -o errexit -xtrace -c "eos mkdir -p ${EOS_FOLDER}/$(dirname {}) && eos cp {} ${EOS_FOLDER}/{} || exit 255"
set +x

kdestroy
