#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
SCRIPTROOT="$(dirname "${BASH_SOURCE[0]:-$0}")/"

source ${SCRIPTROOT}/eos-env.sh

YUM_REPO_REGENERATE=${YUM_REPO_REGENERATE:-}

set -x

# show local directory content, for debugging
ls -la

# ensure remote folder exists (if this is our very first run)
! eos mkdir -p ${EOS_FOLDER}

# failsafe. if any local rpm would overwrite a remote, we must 
# regenerate the repo from scratch
if [[ -z "${YUM_REPO_REGENERATE:-}" ]]; then
  for f in *.rpm; do
    if eos stat ${EOS_FOLDER}${f} 2>/dev/null; then
      echo "'$f' would overwrite existing file ${EOS_FOLDER}${f}. fallback to (slower) full repo metadata generation"
      YUM_REPO_REGENERATE=1
    fi
  done
fi

# setup local data
# if we desire full regeneration of the yum repo db files (slow)
if [[ -n "${YUM_REPO_REGENERATE:-}" ]]; then
  # copy over everything for full repo metadata regeneration
  eos cp -r ${EOS_FOLDER}/* .

# if we are fine with just updating new files (default)
else
  # download existing repodata if present
  if eos stat ${EOS_FOLDER}repodata 2>/dev/null; then
    eos cp -r ${EOS_FOLDER}repodata .
  fi

  if eos stat ${EOS_FOLDER}yumgroups.xml 2>/dev/null; then
    eos cp ${EOS_FOLDER}yumgroups.xml .
  fi
fi

# copy local yumgroups file if specified
if [[ -f "${YUMGROUPS_FILE:-}" ]]; then
  cp ${YUMGROUPS_FILE} yumgroups.xml
fi

# several safeties against garbage data
# gitlab ci can re-use a worker and have this folder present from a past (failed) run
rm -rf .olddata
rm -rf .sys*
rm -rf repodata/.sys*

if [[ -n "${YUM_REPO_REGENERATE:-}" ]]; then
  if [[ -f yumgroups.xml ]]; then
    createrepo -vg yumgroups.xml .
  else
    createrepo -v .
  fi
else
  # create empty file for every missing existing remote rpm
  # to be used with createrepo --update --skip-stat
  eos ls ${EOS_FOLDER}*.rpm | xargs -I {} bash -c 'export F=$(basename {}); touch $F'
  if [[ -f yumgroups.xml ]]; then
    createrepo -vg yumgroups.xml --update --skip-stat .
  else
    createrepo -v --update --skip-stat .
  fi
fi

# if .olddata or .sys files are present during a next run of createrepo, it will fail
# these files must not upload to eos
rm -rf .olddata
rm -rf .sys*
rm -rf repodata/.sys*
# do not upload empty files
find . -size 0 -print -delete

# eos cp -r * $EOS_FOLDER # does not work, '-r' causes access denied error for no good reason, even with owner account and no subfolders
find -type f -printf '%P\0' | xargs -0 -n1 -I {} bash -o errexit -xtrace -c "eos mkdir -p ${EOS_FOLDER}/$(dirname {}) && eos cp {} ${EOS_FOLDER}/{} || exit 255"
set +x

kdestroy
