#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail -o xtrace
IFS=$'\n\t\v'
SCRIPTROOT="$(dirname "${BASH_SOURCE[0]:-$0}")/"

OUTDIR=${1:-${OUTDIR:-"build"}}
case "$OUTDIR" in
*/)
  ;;
*)
  OUTDIR="$OUTDIR/"
  ;;
esac

# print all markdown files to be processed for good measure
find -name '*.md'

# mimic directory structure in /${OUTDIR}
find -name '*.md' -exec bash -o errexit -o nounset -o pipefail -o xtrace -c 'mkdir -p ${2}$(dirname $1)' funcname {} "${OUTDIR}" \;

# copy extra files (images, media, ...)
find -mindepth 2 -name '*.md' ! -path './${OUTDIR}*' -exec bash -o errexit -o nounset -o pipefail -o xtrace -c 'cp -rn $(dirname $1) ${2}$(dirname $(dirname $1))' funcname {} "${OUTDIR}" \;

# run pandoc
find -name '*.md' ! -path './${OUTDIR}*' -exec bash -o errexit -o nounset -o pipefail -o xtrace -c 'pandoc -f gfm -t html5 -o ${2}${1::-3}.html --toc --mathml --standalone --css /pandoc-bootstrap.css --template ${3}pandoc-bootstrap.html {}' funcname {} "${OUTDIR}" "${SCRIPTROOT}" \;

# replace .md links with .html ones
find ${OUTDIR} -name '*.html' -exec bash -o errexit -o nounset -o pipefail -o xtrace -c 'sed -Ei -- "s|href=\"([^\"]+)\\.md\"|href=\"\\1.html\"|g" $1' funcname {} "${OUTDIR}" \;

# README.md = index.html
find ${OUTDIR} -name 'README.html' -exec bash -o errexit -o nounset -o pipefail -o xtrace -c 'cp $1 $(dirname $1)/index.html' funcname {} \;

cp ${SCRIPTROOT}pandoc-bootstrap.css ${OUTDIR}

find ${OUTDIR}