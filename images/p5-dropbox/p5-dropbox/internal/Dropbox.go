package internal

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"golang.org/x/crypto/ssh"
)

// DropboxArgs is used by a dropbox command as input
type DropboxArgs struct {
	Host      string
	OS        string
	Zone      string
	Name      string
	RPMFolder string
}

// GetDropboxArgs generates dropbox arguments
func GetDropboxArgs() (*DropboxArgs, error) {
	dropboxHost := os.Getenv("DROPBOX_HOST")
	if dropboxHost == "" {
		dropboxHost = "cmsdropbox"
	}

	dropboxOS := os.Getenv("DROPBOX_OS")
	if dropboxOS == "" {
		dropboxOS = "cc7"
	}
	dropboxZone := os.Getenv("DROPBOX_ZONE")
	if dropboxZone == "" {
		dropboxZone = "cms"
	}

	dropboxName := os.Getenv("DROPBOX_NAME")
	if dropboxName == "" {
		return nil, fmt.Errorf("no dropbox name given, set DROPBOX_NAME")
	}

	remoteFolder := "/tmp/p5-dropbox"
	if os.Getenv("CI_PROJECT_ID") != "" {
		remoteFolder += "-" + os.Getenv("CI_PROJECT_ID") + "-" + os.Getenv("CI_JOB_ID")
	} else {
		remoteFolder += "-" + fmt.Sprint(time.Now().Unix())
	}

	return &DropboxArgs{
		Host:      dropboxHost,
		OS:        dropboxOS,
		Zone:      dropboxZone,
		Name:      dropboxName,
		RPMFolder: remoteFolder,
	}, nil

}

// Dropbox2 executes the dropbox2 command
func Dropbox2(client *ssh.Client, pass string, job *DropboxArgs) error {
	session, stdin, stdout, stderr, err := SetupSession(client)
	if err != nil {
		return err
	}
	defer session.Close()

	go func() {
		defer (*stdin).Close()
		io.WriteString(*stdin, pass+"\n")
	}()

	// sudo dropbox2 -o cc7 -z cms -s trigger -u p5_rpms
	// # Usage: dropbox2 [-h] [-z ZONE] [-o OS] -s SUBSYSTEM {-u FOLDER | -l | -r SNAPSHOT}'
	// # Performs operations on the SUBSYSTEM's dropbox.
	// #   -h            display this help and exit
	// #   -o OS         can be either 'slc6', 'cc7' or 'cc8' (default is 'cc7')
	// #   -z ZONE       can be either 'cms' or 'cms904' (default is 'cms')
	// #   -s SUBSYSTEM  subsystem dropbox where the packages have to be uploaded.
	// #   -u FOLDER     the RPMs in the FOLDER will be uploaded to the SUBSYSTEM's dropbox
	// #   -l            list the SUBSYSTEM's dropbox SNAPSHOT available
	// #   -r SNAPSHOT   Rollback the SUBSYSTEM's dropbox to the given SNAPSHOT
	c := fmt.Sprintf("sudo --stdin dropbox2 -o %s -z %s -s %s -u %s", job.OS, job.Zone, job.Name, job.RPMFolder)
	log.Printf("Running %s\n", c)
	err = session.Run(c)
	DumpStd(stdout, stderr)

	return err
}
