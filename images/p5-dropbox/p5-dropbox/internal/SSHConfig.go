package internal

import (
	"net"
	"time"

	"golang.org/x/crypto/ssh"
)

// GetSSHConfig generates an ssh config for a specified host
func GetSSHConfig(host string) (*ssh.ClientConfig, string, string) {
	user, pass := GetCredentials(host)
	return &ssh.ClientConfig{
		User:    user,
		Auth:    []ssh.AuthMethod{ssh.Password(pass)},
		Timeout: 30 * time.Second,
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			return nil
		},
	}, user, pass
}
