# Sphinx template

```yaml
documentation:
  stage: stage name
  extends: .auto_devops_sphinx
  variables:
    SOURCE_DIR: docs
  artifacts:
    paths:
    - docs/_build/html
```

This template takes the sphinx documentation in folder specified in `$SOURCE_DIR` and builds it into html files.

A boilerplate set of sphinx files can be generated using `sphinx-quickstart`.
For more info, read the [getting started documentation](https://www.sphinx-doc.org/en/master/usage/quickstart.html) of the sphinx project.

The [Read the Docs Sphinx Theme](https://github.com/readthedocs/sphinx_rtd_theme) is installed by default in the docker image this template uses.

The html files build location depends on your configuration, but is commonly set to `$SOURCE_DIR/_build/html`, and can be used in later pipelines.
For example: to be uploaded to EOS.  