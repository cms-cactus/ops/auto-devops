# XDAQ15 cell builder template

```yaml
build:
  stage: stage name
  extends: .auto_devops_xdaq_15_build
```

This template takes a standard XDAQ15 style makefile, makes it work in CI, and uses it to build the RPMs.

The RPMs will be stored in the folder `ci_rpms` and exposed as artifacts in CI.

By default, this template will only run `make && make rpm`, but is designed to be able to be altered.

```yaml
build:
  stage: stage name
  extends: .auto_devops_xdaq_15_build
  script:
  - make
  - make drivers # for example: the CaloL2 cell has an extra job to build drivers
  - make rpm
```

This template still expects you to supply a XDAQ15 makefile. For example:

```makefile
BUILD_HOME:=$(shell pwd)
Project=cactusprojects
Packages= \
        calol2/swatch \
        calol2/cell

include /opt/cactus/build-utils/mfCommonDefs.mk

include $(XDAQ_ROOT)/build/mfAutoconf.rules
include $(XDAQ_ROOT)/build/mfDefs.$(XDAQ_OS)

include $(XDAQ_ROOT)/build/Makefile.rules
include $(XDAQ_ROOT)/build/mfRPM.rules
```