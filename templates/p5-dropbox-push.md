# P5 Dropbox push template

```yaml
P5 dropbox:
  stage: stage name
  extends: .auto_devops_p5_dropbox_push
  rules:
  - if: $PACKAGE_VER_PATCH || $CI_COMMIT_TAG
    when: manual
  - when: never
  variables:
    LOCAL_FOLDER: build/rpms
    DROPBOX_OS: cc7
    DROPBOX_ZONE: cms
    DROPBOX_NAME: dropbox-name
    SSH_USERNAME: $P5_DROPBOX_USERNAME
    SSH_PASS: $P5_DROPBOX_PASS
```

This template:
- connects to the CMS network via cmsusr.cern.ch
- connects to the P5 dropbox system
- uploads all RPMs in `$LOCAL_FOLDER` to a temporary directory there
- executes the dropbox2 command as described in https://twiki.cern.ch/twiki/bin/viewauth/CMS/DropboxForUsers

The variables `SSH_USERNAME` and `SSH_PASS` are to be closely guarded secrets.
Set them as protected variables; and if you use them in tags, protect your tags so not just anyone can create them.