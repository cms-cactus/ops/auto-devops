# Markdown to html template

```yaml
documentation:
  stage: stage name
  extends: .auto_devops_markdown_to_html
  variables:
    TARGET_DIR: build/docs
  artifacts:
    paths:
    - ./build/docs
```

This template:
- Replicates the project folder structure in `$TARGET_DIR` for any folder that contains markdown files.
- Searches for any markdown files and converts them to HTML in `$TARGET_DIR` using [pandoc](https://pandoc.org/).
- Copies any sibling files next to `*.md` files to `$TARGET_DIR`.
- Replaces any links to `*.md` files with links to `*.html` files.
- Copies any `README.html` files to a sibling `index.html` file.
- Applies a basic theme.

The output in `$TARGET_DIR` can then be used to deploy as a static web site (e.g. EOS).