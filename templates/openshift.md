# CERN OpenShift template

This template enables you to deploy to a [CERN OpenShift](https://openshift.cern.ch/console/catalog) application.

## Quick start

```yaml
# .gitlab-ci.yml

include:
- project: cms-cactus/ops/auto-devops
  ref: 0.2.3
  file: presets/blank-slate.yml

stages:
- 🚀 deploy
- 💣 cleanup

openshift:app:
  stage: 🚀 deploy
  extends: .auto_devops_openshift_basic_app
  # variables:
  #   OPENSHIFT_PROJECT: "projectname"
  #   OPENSHIFT_TOKEN: token
  #   # optional, if your docker image does not listen on port 80
  #   HTTP_PORT: 8000
  #   # optional, custom docker image name
  #   DOCKER_IMAGE_NAME: myimage
  #   # only use for testing purposes, prefer OPENSHIFT_TOKEN instead
  #   OPENSHIFT_USER: "user"
  #   OPENSHIFT_PASS: "pass"
  environment:
    on_stop: 💣 openshift:app

💣 openshift:app:
  stage: 💣 cleanup
  extends: .auto_devops_openshift_basic_app_stop
  dependencies: ["openshift:app"]
```

## Prerequisites

### CERN Website

Create a website on the [CERN Web Services portal](https://webservices.web.cern.ch/webservices/Services/CreateNewSite/Default.aspx).
Specify `PaaS Web Application` as the site type, it will then show up in the [OpenShift console](https://openshift.cern.ch/console/catalog).

### OpenShift credentials

For testing purposes, you can set the environment variables `OPENSHIFT_USER` and `OPENSHIFT_PASS` to your personal account.
However, it's highly recommended to use an access token.

By default, your project already is assigned a `deployer` service account (an openshift account, not CERN).

Go to the [OpenShift console](https://openshift.cern.ch/console/catalog) > Other Resources > Service Account.
For the deployer, select Actions > Edit YAML.
There, you can see the secret name for its token (name: deployer-token-xxxxx).
Go to Resources > Secret > deployer-token-xxxxx, click reveal secret, and copy the `token` field.
This is the token you set as value for the `OPENSHIFT_TOKEN` secret CI variable.

In addition, you will need to grant extra roles to this service account.

Create a yaml file with the following content:
```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: deployer-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: admin
subjects:
- kind: ServiceAccount
  name: deployer
  namespace: my-app-name
```
Note to change the `namespace` key, and apply it using
```bash
oc apply -f deployer-admin.yml
```

### Secure docker container

Check out this article by CERN security: https://security.web.cern.ch/recommendations/en/containers.shtml
It contains a list of best practices.

Openshift enforces a non-root policy. Your docker image will be started with a **random** uid.
Take care that your file permissions in the image can cope with this.
Set the `HTTP_PORT` variable in the openshift template to reflect your http port.
Your image cannot bind to port 1-1024.