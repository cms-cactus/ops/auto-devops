# Upload to GitLab package registry

## Uploading to Gitlab package registry

```yaml
Gitlab registry:
  stage: stage name
  extends: .auto_devops_upload_rpm_gitlab_registry
  variables:
    # folder containing *.rpm files
    RPM_FOLDER: build/rpm
```

This template will upload the RPM files in the RPM_FOLDER for tagged commits.