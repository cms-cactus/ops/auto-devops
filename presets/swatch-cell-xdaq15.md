# XDAQ15 SWATCH Cell preset

```yaml
include:
- project: cms-cactus/ops/auto-devops
  ref: 0.0.8
  file: presets/swatch-cell-xdaq15.yml
```

## features

### automatic builds

This template automatically builds your project using the XDAQ15 makefile in the root of your project.

This template has built-in fixes that one normally needs to perform to make a XDAQ makefile build in CI environments.

### RPM upload to EOS

For this to work, you need to supply credentials to access the EOS API.
Set them as secret variables in the GitLab CI configuration of your project.

The names of these required variables are `$AUTO_DEVOPS_CERNBOX_USER` and `$AUTO_DEVOPS_CERNBOX_PASS`.

If any of the above variables are not set, jobs that need them will simply be
skipped and will not cause your CI pipelines to fail.

### Deploy to P5

This template offers a deploy button in the CI pipeline to deploy to a P5 dropbox.  
This button is only offered in CI pipelines for a git tag.

For this to work, you need to supply credentials to access P5 machines over SSH.
Set them as secret variables in the GitLab CI configuration of your project.
The names of these required variables are `$AUTO_DEVOPS_P5_DROPBOX_NAME`, `$AUTO_DEVOPS_P5_USER`, and `$AUTO_DEVOPS_P5_PASS`.

It is highly recommended to set these variables to 'protected' in the CI configuration.  
If you do, do not forget to also protect your tags. Otherwise, these variables will not be exposed in the CI pipeline for your tag.

If any of the above variables are not set, or you protected your variables but not your tags, jobs that need them will simply be
skipped and will not cause your CI pipelines to fail.

## configuration

You have the option to override any jobs in this preset.
Most notably, the build step is structured to easily add extra make targets.

```yaml
include:
- project: cms-cactus/ops/auto-devops
  ref: 0.0.8
  file: presets/swatch-cell-xdaq15.yml

build:
  script:
  - make calol2-driver
  - make
  - make rpm
```

Or, override publish locations for RPMs in EOS:

```yaml
include:
- project: cms-cactus/ops/auto-devops
  ref: 0.0.8
  file: presets/swatch-cell-xdaq15.yml

publish:RPMs:release:
  variables:
    CERNBOX_FOLDER: /eos/user/c/cactus/www/cactus/release/calol2/$CI_COMMIT_REF_SLUG/centos7_gcc8_x86_64/
```

An empty setup step is provided.
This can be configured to build intermediate Docker images. This is the recommended configuration.


```yaml
builder:
  stage: setup
  extends: .auto_devops_docker_builder_autotag
  variables:
    NAME: builder
    BUILD_ARG_BASE_IMAGE: ${BUILD_IMAGE} # ${BASE_IMAGE} can be invoked in the Dockerfile to expand base images provided by templates
    DOCKERFILE: .gitlab/ci/builder.Dockerfile
    CONTEXT_FOLDER: .gitlab/ci
```