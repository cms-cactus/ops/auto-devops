# This is an experimental preset, does not come with any warranty
# Do not use unless you are willing to encounter problems and report improvements

include:
- local: presets/blank-slate.yml

stages:
- setup
- 🏗️ build
- 🐋 docker image builder
- 🐞 test
- 📦 publish
- 🚀 deploy

variables:
  BUILD_IMAGE: gitlab-registry.cern.ch/cms-cactus/core/swatch/xdaq15-swatch14:tag-v1.6.11
  KUBECTL_IMAGE: gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/kubectl:tag-0.3.8

build:
  stage: 🏗️ build
  extends: .auto_devops_xdaq_15_build
  image: $BUILD_IMAGE

# A builder step to create Docker images that automatically run this Swatch cell on startup. 
# All RPMs are installed and it runs outside the closed network of point 5, but therefore has no network compatibility. 
cell_image_builder:
  stage: 🐋 docker image builder
  extends: .auto_devops_docker_builder_autotag
  variables:
    DOCKERFILE: "__TS_CELL__" # this tells the ci image to use a sepcial Dockerfile which is located at https://gitlab.cern.ch/cms-cactus/ops/auto-devops/-/tree/master/images/kaniko
    BUILD_ARG_SWATCH_IMAGE: ${BUILD_IMAGE}
    BUILD_ARG_XDAQ_PROFILE: "Please set the BUILD_ARG_XDAQ_PROFILE variable" # These variables must be set in the gitlab-ci.yml files of each swatch cell.
    BUILD_ARG_XDAQ_CONFIG: "Please set the BUILD_ARG_XDAQ_CONFIG variable"
    BUILD_ARG_XDAQ_PORT: 3333

test_webpage:
  stage: 🐞 test
  image: $BUILD_IMAGE
  variables:
    XDAQ_PROFILE: "Please set the XDAQ_PROFILE variable" # These variables must be set in the gitlab-ci.yml files of each swatch cell.
    XDAQ_CONFIG: "Please set the XDAQ_CONFIG variable"
    XDAQ_PORT: 3333
  script:
   - sed -i 's|http://linuxsoft.cern.ch/epel/7/|http://linuxsoft.cern.ch/internal/archive/epel/7/|g' /etc/yum.repos.d/epel.repo
   - yum install -y ci_*/*.rpm cmsos-xaas-trigger-extension
   - yum clean all 
# this scripts are added in the https://gitlab.cern.ch/cms-cactus/core/cactus-buildenv/ project
   - bash -c "source /opt/cactus/test/start_ts_cell_background.sh && source /opt/cactus/test/tests/http_status.sh && source /opt/cactus/test/stop_ts_cell.sh"


publish:RPMs:branch:
  stage: 📦 publish
  extends: .auto_devops_upload_yum_repo_eos_template
  rules:
    # omit if user did not supply CERNBox credentials
  - if: $AUTO_DEVOPS_CERNBOX_USER == null || $AUTO_DEVOPS_CERNBOX_PASS == null
    when: never
    # not on tags
  - if: $CI_COMMIT_BRANCH != null
    when: on_success
    allow_failure: false
  variables:
    LOCAL_FOLDER: ci_rpms
    CERNBOX_FOLDER: /eos/user/c/cactus/www/cactus/release/unstable/$CI_PROJECT_PATH_SLUG/$CI_COMMIT_REF_SLUG/$CI_COMMIT_SHA/centos7_gcc8_x86_64/

# BORKEN
# Gitlab registry:
#   stage: 📦 publish
#   extends: .auto_devops_upload_rpm_gitlab_registry
#   variables:
#     # folder containing *.rpm files
#     RPM_FOLDER: ci_rpms

publish:RPMs:release:
  stage: 📦 publish
  extends: .auto_devops_upload_yum_repo_eos_template
  rules:
    # omit if user did not supply CERNBox credentials
  - if: $AUTO_DEVOPS_CERNBOX_USER == null || $AUTO_DEVOPS_CERNBOX_PASS == null
    when: never
    # only on tags
  - if: $CI_COMMIT_TAG != null
    when: on_success
    allow_failure: false
  variables:
    LOCAL_FOLDER: ci_rpms
    CERNBOX_FOLDER: /eos/user/c/cactus/www/cactus/release/$CI_PROJECT_NAME/$CI_COMMIT_REF_SLUG/centos7_gcc8_x86_64/

# In this step, the Docker image created in the cell_image_builder step is deployed to a Kubernetes cluster.
# For this to work, the Kubernetes config yaml file with login information must be saved as a CI variable under the name "K8S_CACTUS_TESTS_CONFIG".
# Also a GitLab deploy token is needed (in this case the username is: "kubernetes-deploy-token" and the password is stored in the CI variable "GITLAB_REGISTRY_TOKEN").
🚀 demo:
  stage: 🚀 deploy
  image: $KUBECTL_IMAGE
  script:
    - >- # check if secret to access the gitlab container registry is already created and if not create it and asign it to the serviceaccount 
      if ! kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" get secret swatch-projects-package-registry; then
        kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" create secret docker-registry swatch-projects-package-registry --docker-server=$CI_REGISTRY --docker-username=kubernetes-deploy-token --docker-password=$GITLAB_REGISTRY_TOKEN
        kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" patch serviceaccount default -p '{"imagePullSecrets": [{"name": "swatch-projects-package-registry"}]}'
      fi
    - >- # check if a deployment of this project is already running and delete it 
      if kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" get deployment "$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG"; then
        kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" delete deployment "$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG"
      fi
    - if [[ -n "${CI_COMMIT_TAG:-}" ]]; then 
        export IMAGE_TAG="tag-${CI_COMMIT_TAG}"; 
      else 
        export IMAGE_TAG="${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}"; 
      fi
    - kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" create deployment "$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG" --image=$CI_REGISTRY_IMAGE:${IMAGE_TAG}    
    - >- # check if a service is already there and if not create one
      if ! kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" get service "$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG"; then
        kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" expose deployment "$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG" --type=NodePort --port=3333
      fi
    - kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" rollout status deployment "$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG"
    - DYNAMIC_ENV_KUBE_HOST=$(kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" get nodes -o go-template --template='{{range .items}}{{index .metadata.labels "kubernetes.io/hostname"}}{{"\n"}}{{end}}' | sed 1q)
    - DYNAMIC_ENV_KUBE_PORT=$(kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" get services "$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG" -o go-template --template="{{range .spec.ports}}{{.nodePort}}{{end}}")
    - echo "Got deployed to $DYNAMIC_ENV_KUBE_HOST.cern.ch:$DYNAMIC_ENV_KUBE_PORT"
    - echo "DYNAMIC_ENV_KUBE_URL=$DYNAMIC_ENV_KUBE_HOST.cern.ch:$DYNAMIC_ENV_KUBE_PORT/urn:xdaq-application:lid=13/" >> deploy.env
  artifacts:
    reports:
      dotenv: deploy.env  
  when: manual
  environment:
    name: demo
    url: http://$DYNAMIC_ENV_KUBE_URL
    on_stop: 💣 stop demo
    deployment_tier: staging

# This step stops the GitLab environment and deletes the kubernetes deployment and service.
💣 stop demo:
  stage: 🚀 deploy
  image: $KUBECTL_IMAGE
  when: manual
  script:
    - kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" delete service "$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG"
    - kubectl --kubeconfig="$K8S_CACTUS_TESTS_CONFIG" delete deployment "$CI_PROJECT_NAME-$CI_ENVIRONMENT_SLUG"
  environment:
    name: demo
    action: stop

P5 dropbox:
  stage: 🚀 deploy
  extends: .auto_devops_p5_dropbox_push
  rules:
    # omit if user did not supply P5 credentials
  - if: $AUTO_DEVOPS_P5_DROPBOX_NAME == null || $AUTO_DEVOPS_P5_USER == null || $AUTO_DEVOPS_P5_PASS == null
    when: never
  - if: $PACKAGE_VER_PATCH || $CI_COMMIT_TAG
    when: manual
    allow_failure: false
  - when: never
  variables:
    LOCAL_FOLDER: ci_rpms
    DROPBOX_OS: cc7
    DROPBOX_ZONE: cms
    DROPBOX_NAME: $AUTO_DEVOPS_P5_DROPBOX_NAME
    SSH_USERNAME: $AUTO_DEVOPS_P5_USER
    SSH_PASS: $AUTO_DEVOPS_P5_PASS
